﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums_1
{

    //enum is short for enumerations. 
    //be default, the first item in the enum list gets assigned a value of 0
    //and each element after that is added +1 to it.
    //this is important becuase, if you were to set the first item a value other than 0
    //rest of the items will have plus one

    //further, I have decided to declare this enum Counters under the namespace
    //instead of the class 
    //this way, another class in the same project or namespace can have access to this enum list without any issues.
    public enum Counters
    {
        one,
        two,
        three,
        four,
    }

    //instead of going with the default assigned values, I can assign any value I want.
    public enum Counters_2
    {
        one = 10,
        two = 20,
        three = 30,
        four = 40,
    }

    class Program
    {


        static void Main(string[] args)
        {
            //displaying the stored values of Counters items
            //you will notice that, when I use like how I have done, the output will not show the actual values of 
            //one or two or three or four as 0, 1, 2 and 3
            //it will simply display the enum values i.e. one, two, three and four
            Debug.WriteLine("Main - Value of one is " + Counters.one);
            Debug.WriteLine("Main - Value of two is " + Counters.two);
            Debug.WriteLine("Main - Value of three is " + Counters.three);
            Debug.WriteLine("Main - Value of four is " + Counters.four);

            //to get the actual values, I have to cast them to int.
            //do note that by default int is the default type for enum

            Debug.WriteLine("Main - Value of one is " + (int)Counters.one);
            Debug.WriteLine("Main - Value of two is " + (int)Counters.two);
            Debug.WriteLine("Main - Value of three is " + (int)Counters.three);
            Debug.WriteLine("Main - Value of four is " + (int)Counters.four);

            //now let me look at another enum list Counters_2
            //which is slightly different than Counters

            Debug.WriteLine("Main - Value of one is " + (int)Counters_2.one);
            Debug.WriteLine("Main - Value of two is " + (int)Counters_2.two);
            Debug.WriteLine("Main - Value of three is " + (int)Counters_2.three);
            Debug.WriteLine("Main - Value of four is " + (int)Counters_2.four);

            
        }
    }
}
